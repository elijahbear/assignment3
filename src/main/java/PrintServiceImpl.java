package assignment3;
import java.util.ArrayList;

public class PrintServiceImpl implements PrintService {
	
	public String print(ArrayList<String> dates, ArrayList<Integer> counts)
	{
		String out = "Year &nbsp;&nbsp;&nbsp;&nbsp; count <br>";
		for(int i = 0; i < dates.size(); i++)
        {
        	out += (dates.get(i) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + counts.get(i).toString() + "<br>");
        }
        return out;
	}
}
