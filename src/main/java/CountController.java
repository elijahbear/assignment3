package assignment3;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class CountController {
	
	private CountService countService;
	private PrintService printService;
	
	public void CountController()
	{

	}
	
	public void setCountService(CountService countService)
	{
		this.countService = countService;
	}
	
	public void setPrintService(PrintService printService)
	{
		this.printService = printService;
	}
	
	@ResponseBody
    @RequestMapping(value = "/meetings", params = {"project"}, method=RequestMethod.GET)
    public String projectCounts(@RequestParam("project") String project)
    {
		ArrayList<String> dates = new ArrayList<String>();
		ArrayList<Integer> counts = new ArrayList<Integer>();
		try{
			dates = countService.getDates(project);
			counts = countService.getCounts(project, dates);
		}
		catch(IOException e)
		{
			return "Unknown project " + project;
		}
        return printService.print(dates, counts);
    }
}