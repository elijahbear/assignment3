package assignment3;

import java.util.ArrayList;
import java.io.IOException;

public interface CountService {
	
	public boolean checkProject(String project);

	public ArrayList<String> getDates(String url) throws IOException;
	
	public ArrayList<Integer> getCounts(String project, ArrayList<String> dates);
}
