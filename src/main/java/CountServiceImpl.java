package assignment3;

import java.util.ArrayList;
import java.util.ListIterator;
import java.net.*;
import java.io.*;

public class CountServiceImpl implements CountService {
	
	
	public boolean checkProject(String project)
	{
    	try{
	    	URL url = new URL("http://eavesdrop.openstack.org/meetings/" + project.replaceFirst("#", ""));
	    	try{
	    		url.getContent();
	    	}
	    	catch(IOException e)
	    	{
	    		return false;
	    	}
	    	return true;
    	}
    	catch(MalformedURLException me)
    	{
    		return false;
    	}
    }
	
	public ArrayList<String> getDates(String project) throws IOException
	{
		ArrayList<String> list = new ArrayList<String>();
		BufferedReader read = getBodyReader(new URL("http://eavesdrop.openstack.org/meetings/" + project.replaceFirst("#", "")));
		String line = null;
		while((line = read.readLine()) != null)
		{
			if(line.matches(".*<a href=\"[0-9]{4}/\">.*"))
			{
				list.add(line.split("<a href=\"[0-9]{4}/\">")[1].split("/")[0]);
			}
		}
		return list;
	}
	
	public ArrayList<Integer> getCounts(String project, ArrayList<String> dates)
	{
		ArrayList<Integer> counts = new ArrayList<Integer>();
		ListIterator<String> itr = dates.listIterator();
		while(itr.hasNext())
		{
			counts.add(getCount(project, itr.next()));
		}
		return counts;
	}
	
	private Integer getCount(String project, String date)
	{
		try
		{
			Integer count = 0;
			String line = null;
			BufferedReader read = getBodyReader(new URL("http://eavesdrop.openstack.org/meetings/" + project.replaceFirst("#", "") + "/" + date));
			while((line = read.readLine()) != null)
			{
				if(line.matches(".*\\.log\\.html.*"))
				{
					count++;
				}
			}
			return count;
		}
		catch(IOException e)
		{
			return 0;
		}
	}
	
	protected BufferedReader getBodyReader(URL url) throws IOException
    {
    	URLConnection urlconn = url.openConnection();
    	return new BufferedReader(new InputStreamReader(urlconn.getInputStream()));
    }

}
