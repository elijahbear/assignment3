package assignment3;
import java.util.ArrayList;

public interface PrintService {
	
	public String print(ArrayList<String> dates, ArrayList<Integer> counts);

}
