package assignment3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.hasItems;

public class TestCountServiceImpl {
	
	CountServiceImpl countService = null;
	
	@Before
	public void setup()
	{
		this.countService = new CountServiceImpl();
	}
	
	@Test
	public void testCheckProjectTrue()
	{
		
		String realProject = "cinder";
		Boolean result = countService.checkProject(realProject);
		assertTrue(result);
		
	}
	
	@Test
	public void testCheckProjectFalse()
	{
		String noProject = "abcdefghijk";
		Boolean result = countService.checkProject(noProject);
		assertFalse(result);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetDates()
	{
		try
		{
			ArrayList<String> dates = countService.getDates("barbican");
			assertThat(dates, hasItems("2013","2014","2015"));
		}
		catch(IOException e)
		{
			assertTrue(false);
		}
		
	}
	
	@Test
	public void testGetCounts()
	{
		ArrayList<String> dates = new ArrayList<String>();
		dates.add("2013");
		dates.add("2014");
		dates.add("2015");
		ArrayList<Integer> counts = countService.getCounts("barbican", dates);
		assertSame(counts.get(0), 1);
		assertSame(counts.get(1), 43);
	}
	
	

}
