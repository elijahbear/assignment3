package assignment3;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestPrintServiceImpl {
	
	PrintServiceImpl printService = null;
	
	@Before
	public void setup()
	{
		this.printService = new PrintServiceImpl();
	}
	
	@Test
	public void testPrint()
	{
		ArrayList<String> dates = new ArrayList<String>();
		dates.add("2012");
		dates.add("2013");
		dates.add("2014");
		ArrayList<Integer> counts = new ArrayList<Integer>();
		counts.add(2);
		counts.add(3);
		counts.add(4);
		String out = printService.print(dates, counts);
		String expected = "Year &nbsp;&nbsp;&nbsp;&nbsp; count<br>2012&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2<br>2013&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3<br>2014&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4<br>";
		
		assertEquals(out, expected);
	}

}
